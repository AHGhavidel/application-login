package com.example.icandoit;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.icandoit.DataModel.User;

import java.util.ArrayList;
import java.util.List;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.userViewHolder> {
    List<User> users  = new ArrayList<User>();

    public UserAdapter() { }

    @NonNull
    @Override
    public userViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user,
                parent, false);
        return new userViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull userViewHolder holder, int position) {
        holder.bindUser(users.get(position));
    }

    public void addItems(List<User> users){
        this.users.addAll(users);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() { return users.size(); }

    public class userViewHolder extends RecyclerView.ViewHolder{

        private TextView tv_first, tv_Username, tv_email, tv_password;

        public userViewHolder(@NonNull View itemView) {
            super(itemView);

            tv_first = itemView.findViewById(R.id.tv_item_user);
            tv_Username = itemView.findViewById(R.id.tv_item_Username);
            tv_email = itemView.findViewById(R.id.tv_item_email);
            tv_password = itemView.findViewById(R.id.tv_item_password);
        }

        public void bindUser(final User user){
            tv_Username.setText(user.getUsername());
            tv_first.setText(user.getUsername().substring(0,1));
            tv_email.setText(user.getEmail());
            tv_password.setText(user.getPassword());
        }
    }
}
