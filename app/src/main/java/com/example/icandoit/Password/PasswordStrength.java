package com.example.icandoit.Password;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.icandoit.R;

public enum PasswordStrength {
    WEAK(R.string.weak, Color.RED),
    MEDIUM(R.string.medium, Color.argb(255, 220, 185, 0)),
    STRONG(R.string.strong, Color.GREEN),
    VERY_STRONG(R.string.very_strong, Color.BLUE);

    public int msg;
    public int color;
    private static int MIN_LENGTH = 8;
    private static int MAX_LENGTH = 15;

    PasswordStrength(int msg, int color) {
        this.msg = msg;
        this.color = color;
    }

    public CharSequence getText(android.content.Context context) {
        return context.getText(msg);
    }

    public int getColor() {
        return color;
    }

    public static PasswordStrength calculate(String password) {

        int length = password.length();
        int score = 0;

        boolean upper = false;
        boolean lower = false;
        boolean digit = false;
        boolean specialChar = false;

        if( password.matches("(?=.*[0-9]).*") )
            score++;
        digit = true;

        if( password.matches("(?=.*[a-z]).*") )
            lower = true;

        if( password.matches("(?=.*[A-Z]).*") )
            upper = true;

        if( password.matches("(?=.*[~!@#$%^&*()_-]).*") ) {
            score++;
            specialChar = true;
        }

        if(lower == true & upper == true){
            score++;
        }

        if (length > MAX_LENGTH) {
            score++;
        }
        else if (length < MIN_LENGTH) {
            score = 0;
        }

        switch (score){
            case 0: return WEAK;
            case 1: return MEDIUM;
            case 2: return STRONG;
            case 3: return VERY_STRONG;
        }

        return VERY_STRONG;
    }

    public static void stProgressBar(Context context, ProgressBar progressBar,
                                     PasswordStrength passwordStrength, TextView textView){

        textView.setVisibility(View.VISIBLE);
        textView.setText(passwordStrength.getText(context));
        textView.setTextColor(passwordStrength.getColor());

        progressBar.setVisibility(View.VISIBLE);

        progressBar.getProgressDrawable().setColorFilter(passwordStrength.getColor(),
                PorterDuff.Mode.SRC_IN);
        switch(passwordStrength){
            case WEAK:
                progressBar.setProgress(25);
            case MEDIUM:
                progressBar.setProgress(50);
            case STRONG:
                progressBar.setProgress(75);
            case VERY_STRONG:
                progressBar.setProgress(100);
        }

    }
}
