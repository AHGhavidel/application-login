package com.example.icandoit.DataModel;

public class User {
    private String username;
    private String email;
    private String password;

    public User(String uName, String email, String password) {
        this.username = uName;
        this.email = email;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }
    public String getEmail() {
        return email;
    }
    public String getPassword() { return password; }
}
