package com.example.icandoit.DataBase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import com.example.icandoit.DataModel.User;

import java.util.ArrayList;
import java.util.List;

public class UserDb extends SQLiteOpenHelper {
    //Information Database
    private static final String DATABASE_NAME =  "UserDB";
    private static final int DATABASE_VERSION = 1;
    private final SQLiteDatabase sqLiteDatabaseWrite = getWritableDatabase();
    private final SQLiteDatabase sqLiteDatabaseRead = getReadableDatabase();


    //Table Name
    public static final String TABLE_NAME = "User";

    //Column InfO
    public static final String COL_1 = "Username";
    public static final String COL_2 = "Email";
    public static final String COL_3 = "Password";



    public UserDb(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(" CREATE TABLE " + TABLE_NAME + " (Username TEXT PRIMARY KEY, " +
                "Email TEXT NOT NULL, Password TEXT NOT NULL); ");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public boolean insertUser(User user){
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_1, user.getUsername());
        contentValues.put(COL_2, user.getEmail());
        contentValues.put(COL_3, user.getPassword());
        long result = sqLiteDatabaseWrite.insert(TABLE_NAME, null ,contentValues);
        if(result == -1){
            return false;
        }
        else{
            return true;
        }
    }

    public int updateUser(String username, User user){
        if(sqLiteDatabaseWrite.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE " +
                COL_1 + " LIKE'%" + user.getUsername() + "%'", null).getCount() > 0){
            return 2;
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_1, user.getUsername());
        contentValues.put(COL_2, user.getEmail());
        contentValues.put(COL_3, user.getPassword());
        long result = sqLiteDatabaseWrite.update(TABLE_NAME, contentValues, "Username = ?" ,
                new String[] {username});
        if(result <= 0){
            return 0;
        }
        else{
            return 1;
        }
    }

    public List<User> getAll(){
        List<User> users = new ArrayList<User>();
        Cursor result = sqLiteDatabaseRead.rawQuery("SELECT * FROM " + TABLE_NAME,
                null);
        if(result.moveToFirst()){
            do{
                User user = new User(result.getString(0), result.getString(1),
                        result.getString(2));
                users.add(user);
            }
            while(result.moveToNext());
            return users;
        }
        return null;
    }

    public Cursor showUsername(String username){
        return sqLiteDatabaseRead.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE " +
                COL_1 + " LIKE'%" + username + "%'", null);
    }

    public boolean deleteUser(String username){
        Integer result = sqLiteDatabaseWrite.delete(TABLE_NAME, "Username = ?",
                new String[] {username});
        final boolean check = result > 0;
        return check;
    }

    public boolean delAll(){
        Integer result = sqLiteDatabaseWrite.delete(TABLE_NAME, null, null);
        final boolean check = result > 0;
        return check;
    }
}