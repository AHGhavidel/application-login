package com.example.icandoit.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.icandoit.DataModel.User;
import com.example.icandoit.R;
import com.example.icandoit.DataBase.UserDb;
import com.example.icandoit.Password.PasswordStrength;
import com.google.android.material.textfield.TextInputEditText;

import java.util.regex.Pattern;

import es.dmoral.toasty.Toasty;



public class Add<USERNAME_REGEX> extends AppCompatActivity {

    UserDb myDb;

    User user;
    private static final String TAG = "Add";
    private TextInputEditText et_Username, et_Email, et_Password;

    private TextView tv_Strength_Password, tv_Valid_Email, tv_Valid_Username;

    private View btn_Add;

    private ProgressBar pb_Strength_Password;

    static final String USERNAME_REGEX = "^(?=.{8,20}$)(?![_.])(?!.*[_.]{2})" +
            "[a-zA-Z0-9._]+(?<![_.])$";
    static final String EMAIL_REGEX = "^[a-zA-Z0-9_+&*-]+(?:\\.[a-zA-Z0-9_+&*-]+)*" +
            "@" + "(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$";
    static final String PASSWORD_REGEX = "^(?=.*[0-9])(?=.*[a-z]).{8,16}$";

    static final Pattern USERNAME_PATTERN = Pattern.compile(USERNAME_REGEX);
    static final Pattern EMAIL_PATTERN = Pattern.compile(EMAIL_REGEX);
    static final Pattern PASSWORD_PATTERN = Pattern.compile(PASSWORD_REGEX);



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        myDb = new UserDb(this);
        setContentView(R.layout.activity_add);

        //initView

        tv_Valid_Username = findViewById(R.id.tv_add_valid_username);
        tv_Valid_Email = findViewById(R.id.tv_add_valid_email);
        tv_Strength_Password = findViewById(R.id.tv_add_valid_password);

        et_Username = findViewById(R.id.et_add_username);
        et_Email = findViewById(R.id.et_add_email);
        et_Password = findViewById(R.id.et_add_password);

        btn_Add = findViewById(R.id.btn_sign_up);

        pb_Strength_Password = findViewById(R.id.pb_add_strength_password);

        //

        btn_Add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addUser();
            }
        });

        et_Email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(!EMAIL_PATTERN.matcher(charSequence.toString()).matches()) {
                    tv_Valid_Email.setVisibility(View.VISIBLE);
                }
                else{
                    tv_Valid_Email.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) { }
        });

        et_Username.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(!USERNAME_PATTERN.matcher(charSequence.toString()).matches()) {
                    tv_Valid_Username.setVisibility(View.VISIBLE);
                }
                else{
                    tv_Valid_Username.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        et_Password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                PasswordStrength.stProgressBar(getApplicationContext(), pb_Strength_Password,
                        PasswordStrength.calculate(charSequence.toString()), tv_Strength_Password);
            }

            @Override
            public void afterTextChanged(Editable editable) { }
        });
    }

    private void addUser(){
        String username = et_Username.getText().toString();
        String email = et_Email.getText().toString();
        String password = et_Password.getText().toString();

        if(username.isEmpty()){
            Toasty.warning(getApplicationContext(), R.string.toast_enter_username,
                    Toasty.LENGTH_SHORT).show();
            return;
        }

        if(!USERNAME_PATTERN.matcher(username).matches()){
            Toasty.warning(getApplicationContext(), R.string.invalid_username,
                    Toasty.LENGTH_SHORT).show();
            return;
        }

        if(email.isEmpty()){
            Toasty.warning(getApplicationContext(), R.string.toast_enter_email,
                    Toasty.LENGTH_SHORT).show();
            return;
        }

        if(!EMAIL_PATTERN.matcher(email).matches()){
            return;
        }

        if(password.isEmpty()){
            Toasty.warning(getApplicationContext(), R.string.toast_enter_password,
                    Toasty.LENGTH_SHORT).show();
            return;
        }

        if(!PASSWORD_PATTERN.matcher(password).matches()){
            return;
        }

        user = new User(username, email, password);

        if(myDb.insertUser(user)){
            Toasty.success(getApplicationContext(), R.string.toast_successful,
                    Toasty.LENGTH_SHORT).show();
            et_Username.setText("");
            et_Email.setText("");
            et_Password.setText("");

            tv_Valid_Username.setVisibility(View.INVISIBLE);
            tv_Valid_Email.setVisibility(View.INVISIBLE);
            tv_Strength_Password.setVisibility(View.INVISIBLE);

            pb_Strength_Password.setVisibility(View.INVISIBLE);

            return;
        }
        else{
            Toasty.warning(getApplicationContext(), R.string.toast_available,
                    Toasty.LENGTH_SHORT).show();
        }
    }

}