package com.example.icandoit.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.example.icandoit.R;
import com.example.icandoit.DataBase.UserDb;

import es.dmoral.toasty.Toasty;

public class Delete extends AppCompatActivity {

    UserDb myDb;

    private EditText et_Username;

    private View btn_DeleteUser, btn_DeleteAll;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete);

        myDb = new UserDb(this);

        et_Username= findViewById(R.id.et_delete_user_username);
        btn_DeleteUser = findViewById(R.id.btn_delete_user_del);
        btn_DeleteAll = findViewById(R.id.btn_delete_delete_all);

        //initView

        btn_DeleteUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { delUser(et_Username.getText().toString()); }
        });

        btn_DeleteAll.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){ delAll(); }
        });

    }
    public void delAll(){
        if(myDb.delAll()){
            Toasty.success(getApplicationContext(), R.string.toast_successful,
                    Toasty.LENGTH_SHORT).show();
            return;
        }
        else{
            Toasty.warning(getApplicationContext(), R.string.toast_empty,
                    Toasty.LENGTH_LONG).show();
            return;
        }
    }

    public void delUser(String username){
        if(username.isEmpty()){
            Toasty.warning(getApplicationContext(), R.string.toast_enter_username,
                    Toasty.LENGTH_SHORT).show();
            return;
        }

        if(myDb.deleteUser(username)){
            Toasty.success(getApplicationContext(), R.string.toast_successful,
                    Toasty.LENGTH_SHORT).show();
            et_Username.setText("");
        } else{
            Toasty.warning(getApplicationContext(), R.string.invalid_username,
                    Toasty.LENGTH_LONG).show();
        }
        return;
    }
}