package com.example.icandoit.Activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.example.icandoit.R;
import com.example.icandoit.DataBase.UserDb;
import com.example.icandoit.UserAdapter;

import es.dmoral.toasty.Toasty;

public class Show extends AppCompatActivity {

    UserDb myDb;
    AlertDialog.Builder builder;
    UserAdapter userAdapter;

    private EditText et_username;

    private View btn_show, btn_show_all;

    private RecyclerView rv_users;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show);

        //initView
        myDb = new UserDb(this);

        userAdapter = new UserAdapter();

        et_username = findViewById(R.id.et_edit_user_username);

        btn_show = findViewById(R.id.btn_show_showuser);
        btn_show_all = findViewById(R.id.btn_show_show_all);

        rv_users = findViewById(R.id.rv_show_users);


        //Button and ...

        rv_users.setLayoutManager(new LinearLayoutManager(this , RecyclerView.VERTICAL,
                false));
        rv_users.setAdapter(userAdapter);

        btn_show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showUser(et_username.getText().toString());
            }
        });

        btn_show_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getAll();
            }
        });

    }
    private void alertDialog(String message){
        builder = new AlertDialog.Builder(this);

        builder.setIcon(R.drawable.ic_user_24dp);
        builder.setTitle(R.string.users);
        builder.setMessage(message);

        AlertDialog alertDialog = builder.create();
        builder.show();
    }

    private void showUser(String username){
        if(username.isEmpty()){
            Toasty.warning(getApplicationContext(), R.string.toast_enter_username,
                    Toasty.LENGTH_SHORT).show();
            return;
        }
        Cursor result = myDb.showUsername(username);

        StringBuffer stringBuffer = new StringBuffer();

        if(result.getCount() > 0){

            stringBuffer.append(R.string.username + " : " + result.getString(0) + "\n");
            stringBuffer.append(R.string.email + " : " + result.getString(1) + "\n");
            stringBuffer.append(R.string.password + " : " + result.getString(2) + "\n");
            stringBuffer.append("................................................." + "\n");


            alertDialog(stringBuffer.toString());
        } else{
            Toasty.warning(getApplicationContext(), R.string.invalid_username,
                    Toasty.LENGTH_SHORT).show();
        }
    }

    private void getAll(){
        if((myDb.getAll()) == null){
            Toasty.warning(getApplicationContext(), R.string.toast_empty,
                    Toasty.LENGTH_SHORT).show();
            return;
        }
        userAdapter.addItems(myDb.getAll());
        rv_users.setVisibility(View.VISIBLE);
    }
}