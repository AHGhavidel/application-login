package com.example.icandoit.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.example.icandoit.R;

public class SplashScreen extends AppCompatActivity {

    ImageView img_SplashScreen;
    Animation animation_Rotate;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        img_SplashScreen = findViewById(R.id.img_splashscreen);

        animation_Rotate = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.animation);
        img_SplashScreen.setVisibility(View.VISIBLE);
        img_SplashScreen.startAnimation(animation_Rotate);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashScreen.this, MainActivity.class);
                startActivity(intent);

                finish();
            }
        }, 1400);
    }
}