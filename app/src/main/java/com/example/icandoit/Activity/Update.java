package com.example.icandoit.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.icandoit.DataModel.User;
import com.example.icandoit.R;
import com.example.icandoit.DataBase.UserDb;
import com.example.icandoit.Password.PasswordStrength;


import es.dmoral.toasty.Toasty;

import static com.example.icandoit.Activity.Add.EMAIL_PATTERN;
import static com.example.icandoit.Activity.Add.PASSWORD_PATTERN;
import static com.example.icandoit.Activity.Add.USERNAME_PATTERN;

public class Update extends AppCompatActivity {

    UserDb myDb;

    User user;

    private boolean uses;

    private EditText et_Username, et_New_Username, et_New_Email, et_New_Password;

    private View btn_Update;

    private TextView tv_Valid_Email , tv_Strength_Password, tv_Valid_NewUsername;

    private ProgressBar pb_Strength_Password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);

        //initView
        tv_Valid_Email = findViewById(R.id.tv_update_valid_email);
        tv_Strength_Password = findViewById(R.id.tv_update_valid_password);
        tv_Valid_NewUsername = findViewById(R.id.tv_update_valid_newusername);

        myDb = new UserDb(this);

        et_Username = findViewById(R.id.et_update_username);
        et_New_Username = findViewById(R.id.et_update_new_username);
        et_New_Email = findViewById(R.id.et_update_new_email);
        et_New_Password = findViewById(R.id.et_update_new_password);

        btn_Update = findViewById(R.id.btn_update_update);

        pb_Strength_Password = findViewById(R.id.pb_update_strength_password);

        //Button and EditText

        et_New_Username.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(!USERNAME_PATTERN.matcher(charSequence.toString()).matches()) {
                    tv_Valid_NewUsername.setVisibility(View.VISIBLE);
                }
                else{
                    tv_Valid_NewUsername.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        et_New_Email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(!EMAIL_PATTERN.matcher(charSequence.toString()).matches())
                    tv_Valid_Email.setVisibility(View.VISIBLE);
                else{
                    tv_Valid_Email.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) { }
        });

        et_New_Password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                PasswordStrength.stProgressBar(getApplicationContext(), pb_Strength_Password,
                        PasswordStrength.calculate(charSequence.toString()), tv_Strength_Password);
            }

            @Override
            public void afterTextChanged(Editable editable) { }
        });

        btn_Update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateUser(et_Username.getText().toString(), et_New_Username.getText().toString(),
                        et_New_Email.getText().toString(), et_New_Password.getText().toString());
            }
        });



    }
    public void updateUser(String username, String newUsername, String newEmail, String newPassword){
        if(username.isEmpty()){
            Toasty.warning(getApplicationContext(), R.string.toast_enter_username,
                    Toasty.LENGTH_SHORT).show();
            return;
        }

        if(newUsername.isEmpty()){
            Toasty.warning(getApplicationContext(), R.string.toast_enter_newusername,
                    Toasty.LENGTH_SHORT).show();
            return;
        }

        if(!USERNAME_PATTERN.matcher(newUsername).matches()){
            Toasty.warning(getApplicationContext(), R.string.invalid_username,
                    Toasty.LENGTH_SHORT).show();
            return;
        }

        if(newEmail.isEmpty()){
            Toasty.warning(getApplicationContext(), R.string.toast_enter_email,
                    Toasty.LENGTH_SHORT).show();
            return;
        }

        if(!EMAIL_PATTERN.matcher(newEmail).matches()){
            return;
        }

        if(newPassword.isEmpty()){
            Toasty.warning(getApplicationContext(), R.string.toast_enter_password,
                    Toasty.LENGTH_SHORT).show();
            return;
        }

        if(!PASSWORD_PATTERN.matcher(newPassword).matches()){
            return;
        }

        user = new User(newUsername, newEmail, newPassword);

        switch (myDb.updateUser(username, user)){
            case 0:
                Toasty.warning(getApplicationContext(), R.string.invalid_username,
                        Toasty.LENGTH_SHORT).show();
                return;
            case 1:
                et_Username.setText("");
                et_New_Username.setText("");
                et_New_Email.setText("");
                et_New_Password.setText("");

                tv_Valid_NewUsername.setVisibility(View.INVISIBLE);
                tv_Valid_Email.setVisibility(View.INVISIBLE);
                tv_Strength_Password.setVisibility(View.INVISIBLE);

                pb_Strength_Password.setVisibility(View.INVISIBLE);

                Toasty.success(getApplicationContext(), R.string.toast_successful,
                        Toasty.LENGTH_SHORT).show();
                return;
            case 2:
                Toasty.warning(getApplicationContext(), R.string.available_newusername,
                        Toasty.LENGTH_SHORT).show();
        }
    }
}